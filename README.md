# Simple Text Editor

## Command Line Arguments

Arguments can be passed into the program includinng an optional file name and an optional -p to set the default prompt.

This allows the program to be launched many ways :

```sh
# To launch the program
ruby Editor.rb

# To automatically open a file at launch
ruby Editor.rb some_file.txt

# To set a default prompt on launch
ruby Editor.rb -p "prompt "

# To set a prompt and automatically open a file at launch
ruby Editor.rb -p "prompt " some_file.txt
```

Custom prompts passed through the command line as arguments will not be padded with a space unlike those set within the program.

I currently do not have any plans to change this or add any more flags.. stranger things have been known to happen though.

## Commands

There are a multitude of commands that can be used from within the program to edit files, all of which are detailed below in alphabetical order, all commands are currently case sensitive.

Command | Name | Details
-|-|-
a | Append | Can be invoked to append after the current buffer location, halted with the . command
b | Buffer | Can be invoked to print out the current buffer location, also prints that line
c | Change | Can be invoked to overwrite at the current buffer location, halted with the . command
d | Delete | Can be invoked to delete a line or a range of lines, inclusive, from the buffer with `d <line>` and `d <from> <to>` respectively
e | Edit | Can be invoked to load a file into the buffer `e <file>`
f | File | Can be invoked to print the current file name or to alter it `f <name>`
g | Grep | Can be invoked to find lines containing a given string, no regex, `g <string>`
h | Help | Not yet implemented
l | List | Can be invoked to list the current buffer `l`, a relative portion of it `l <+-number>` or a range of lines, inclusive, `l <from> <to>`
m | Move | Not yet implemented - Intended plan is move a range of lines after another
n | List Numeric | Does the same as `l` but with indexed line numbers
p | Prompt | Used to turn the default prompt on `p` or set a custom prompt with `p <char>`
r | Replace | Not yet implemented
s | Sed | Can be invoked to change the first occurence of a string to another on the current buffer line with `s/from/to/`
t | Transfer | Not yet implemented - Intended plan is copy a range of lines after another
u | Undo | Not yet implemented
w | Write | Can be invoked to write the contents of the buffer to the current file
! | Shell Escape | Can be invoked to run shell commands within the program
Any number | Index | Moves the current buffer location to the line you request

Any noticed error will return a solitary `?`, these may become more verbose or they may not.. who knows.