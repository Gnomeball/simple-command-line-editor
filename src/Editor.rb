#!/Users/gnome/.rvm/rubies/ruby-2.6.3/bin/ruby

# Initialise any command line arguments passed to the program
def init_args

    if 1 == ARGV.length
        # File name
        open_file(ARGV[0])
    elsif 2 == ARGV.length
        # -p <prompt>
        @default_prompt = ARGV[1]
    elsif 3 == ARGV.length
        # File name
        open_file(ARGV[2])
        # -p <prompt>
        @default_prompt = ARGV[1]
    end

    (1..ARGV.length).each { ARGV.pop }

end

# Open a file and replace the buffer with it's contents
def open_file(name)
    @file_name = name
    File.exists?(@file_name) ? @buffer = File.read(@file_name).split("\n") : File.new(@file_name, 'w')
    @current_position = @buffer.length
    puts File.size(@file_name)
end

# Sets a file name
def set_file_name(args)
    (1 == args.length) ? @file_name = @default_file_name : @file_name = args[1]
    puts @file_name
end

# Lists the buffer
def list_buffer(args, index)
    from = 1
    to = @buffer.length
    if 2 == args.length
        # L and a modifier
        modifier = args[1].to_i
        from = @current_position
        to = @current_position
        (modifier < 0) ? from = to + modifier : to = from + modifier
    elsif 3 == args.length
        # L and a range
        from = args[1].to_i
        to = args[2].to_i
    end
    to = @buffer.length if to > @buffer.length
    from = 1 if from < 1
    if to >= from
        (from..to).each { |i| index ? (puts "#{i.to_s.ljust(8)}#{@buffer[i - 1]}") : (puts "#{@buffer[i - 1]}") }
    else
        error
    end
end

# Moves the current buffer position
def move_buffer(pos)
    @current_position = pos
    puts @buffer[@current_position-1]
end

# Appends after the current buffer location
def append_buffer
    loop do
        line = gets
        break if line.start_with? '.'
        @buffer.insert(@current_position, line[0..-2])
        @current_position += 1
    end
end

# Appends at and over the current buffer position
def change_buffer
    loop do
        line = gets
        break if line.start_with? '.'
        @buffer[@current_position-1] = line[0..-2]
        @current_position += 1
    end
end

# Deletes a line from the buffer
def delete_line(n)
    if n <= @buffer.length
        @buffer.delete_at(n-1)
        @current_position -= 1 if @current_position > @buffer.length
    end
end

# Delete a range of lines from the buffer
def delete_lines_from_buffer(args)
    from = args[1].to_i
    if 2 == args.length
        # Delete a single line
        delete_line(from)
    elsif 3 == args.length
        # Delete a range of lines
        to = args[2].to_i
        (from..to).reverse_each{ |i| delete_line(i) }
    end
end

# Rubbish grep
def get_lines(arg)
    (1..@buffer.length).each { |i| puts "#{i.to_s.ljust(8)}#{@buffer[i - 1]}" if @buffer[i - 1].include? arg }
end

# Rubbish sed
def substitute(args)
    @buffer[@current_position - 1][args[1]] = args[2]
end

# Writes the contents of the buffer to file
def write_to_file
    @file_name = @default_file_name if @file_name.empty?
    file = File.open(@file_name, 'w')
    @buffer.each { |line| file.write "#{line}\n" }
    file.close
    puts File.size(@file_name)
end

# Do a system call
def do_system_call(args)
    system(args)
    puts '!'
end

# If in doubt.. question mark!
def error
    puts '?'
end

# Runs stuff
def main

    @default_prompt = "* "
    @default_file_name = "file"
    @prompt
    @file_name = @default_file_name
    @buffer = []
    @current_position = 0

    init_args if ARGV.length > 0

    loop do

        print @prompt
        input = gets
        break if input[0] == 'q'

        case input[0]
        when "a" then append_buffer
        when "b" then puts "#{@current_position.to_s.ljust(8)}#{@buffer[@current_position - 1]}"
        when "c" then change_buffer
        when "d" then delete_lines_from_buffer(input.split(' '))
        when "e" then (1 == input.split(' ').length) ? open_file(@file_name) : open_file(input.split(' ')[1])
        when "f" then set_file_name(input.split(' '))
        when "g" then get_lines(input.split(' ')[1])
        when "h" then # help?
        when "l" then list_buffer(input.split(' '), false)
        when "m" then # move after
        when "n" then list_buffer(input.split(' '), true)
        when "p" then @prompt = 1 == input.split(' ').length ? @default_prompt : "#{input.split(' ')[1]} "
        when "r" then # replace
        when "t" then # transfer after (copy)
        when "s" then substitute(input.split('/'))
        when "u" then # undo
        when "w" then write_to_file
        when "!" then do_system_call(input[1..-1])
        when "\n" then error
        else
            # Any number, move the buffer
            (input.split(' ')[0].to_i < @buffer.length + 1) ? move_buffer(input.split(' ')[0].to_i) : error if input[0].to_i.is_a?(Integer)
        end

    end

end

main